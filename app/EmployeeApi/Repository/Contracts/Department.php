<?php
/**
 * Created by PhpStorm.
 * User: Gayan Yapa
 * Date: 6/25/2016
 * Time: 8:55 AM
 */

namespace App\EmployeeApi\Repository\Contracts;


interface Department
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param int $id
     * @return \App\EmployeeApi\Entity\Department
     */
    public function getById($id);

    /**
     * @param \App\EmployeeApi\Entity\Department $department
     * @return \App\EmployeeApi\Entity\Department
     */
    public function save(\App\EmployeeApi\Entity\Department $department);

    /**
     * @param \App\EmployeeApi\Entity\Department $department
     */
    public function update(\App\EmployeeApi\Entity\Department $department);

    /**
     * Returns list of employee fot the department
     * @param int $id Department Id
     * @return mixed
     */
    public function getEmployees($id);

}